var wms_layers = [];
var baseLayer = new ol.layer.Group({
    'title': '',
    layers: [
new ol.layer.Tile({
    'title': 'OSM',
    'type': 'base',
    source: new ol.source.OSM()
})
]
});
var format_dsfddsf_0 = new ol.format.GeoJSON();
var features_dsfddsf_0 = format_dsfddsf_0.readFeatures(json_dsfddsf_0, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_dsfddsf_0 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_dsfddsf_0.addFeatures(features_dsfddsf_0);var lyr_dsfddsf_0 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_dsfddsf_0, 
                style: style_dsfddsf_0,
                title: '<img src="styles/legend/dsfddsf_0.png" /> dsfddsf'
            });

lyr_dsfddsf_0.setVisible(true);
var layersList = [baseLayer,lyr_dsfddsf_0];
lyr_dsfddsf_0.set('fieldAliases', {'id': 'id', 'Nombre': 'Nombre', 'Ubicacion': 'Ubicacion', });
lyr_dsfddsf_0.set('fieldImages', {'id': 'TextEdit', 'Nombre': 'TextEdit', 'Ubicacion': 'TextEdit', });
lyr_dsfddsf_0.set('fieldLabels', {'id': 'no label', 'Nombre': 'no label', 'Ubicacion': 'no label', });
lyr_dsfddsf_0.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});